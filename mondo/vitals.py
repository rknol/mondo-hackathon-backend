class VitalService:
    def __init__(self):
        self.images_matrix = {
            'top': {
                'normal': 0,
                'asleep': 1,
                'drunk': 2
            },
            'bottom': {
                'skinny': 0,
                'normal': 1,
                'fat': 2
            }
        }

        self.vitals = {
            'fat': 5,
            'caffeine': 5,
            'alcohol': 0,
            'happy': 2
        }

        self.threshold = {
            'fat': [3, 20],
            'caffeine': 3,
            'alcohol': 5,
        }

        self.transactions = []

    def append_transaction(self, tx):
        self.transactions.append(tx)

    def get_top_img(self):
        state = 'normal'

        if self.vitals['caffeine'] < self.threshold['caffeine']:
            state = 'asleep'
        if self.vitals['alcohol'] > self.threshold['alcohol']:
            # Drunk overrides asleep
            state = 'drunk'

        return self.images_matrix['top'][state]

    def get_bottom_img(self):
        state = 'normal'

        if self.vitals['fat'] < self.threshold['fat'][0]:
            state = 'skinny'
        if self.vitals['fat'] > self.threshold['fat'][1]:
            state = 'fat'

        return self.images_matrix['bottom'][state]

    def calculate(self):
        for tx in self.transactions:
            # Bring down vitals over time
            if self.vitals['fat'] > 0:
                self.vitals['fat'] -= 1
            if self.vitals['caffeine'] > 0:
                self.vitals['caffeine'] -= 1
            if self.vitals['alcohol'] > 0:
                self.vitals['alcohol'] -= 1
            if self.vitals['happy'] > 0:
                self.vitals['happy'] -= 1

            # Append the totals of the defined transaction's
            # merchant vitals to the current vital stats
            merchant = tx.merchant
            self.vitals['fat'] += merchant.fat
            self.vitals['caffeine'] += merchant.caffeine
            self.vitals['alcohol'] += merchant.alcohol

            happy = self.vitals['happy'] + merchant.happy
            if happy > 4:
                self.vitals['happy'] = 4
            else:
                self.vitals['happy'] = happy

        # Build the matrix
        return {
            'status': 0,
            'vitals': self.vitals,
            'top_img': self.get_top_img(),
            'bottom_img': self.get_bottom_img(),
            'happy': self.vitals['happy']
        }