from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime, Text, Float
from sqlalchemy.orm import relationship
from datetime import datetime

Base = declarative_base()


class Merchant(Base):
    __tablename__ = "merchants"
    id = Column(Integer, primary_key=True)
    transactions = relationship("Transaction", back_populates="merchant")

    merchant_id = Column(String(255))
    name = Column(String(255))

    # Vital modifiers
    fat = Column(Integer)
    alcohol = Column(Integer)
    caffeine = Column(Integer)
    happy = Column(Integer)

    def __repr__(self):
        return "<Merchant '{}'>".format(self.name)


class Transaction(Base):
    __tablename__ = "transactions"

    id = Column(Integer, primary_key=True)
    merchant_id = Column(Integer, ForeignKey('merchants.id'))
    merchant = relationship("Merchant", back_populates="transactions")

    tx_id = Column(String(255))
    amount = Column(Float)
    created = Column(DateTime, default=datetime.now)

    def __repr__(self):
        return "<Transaction '{}'>".format(self.tx_id)