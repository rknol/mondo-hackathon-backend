from .extensions import ApiView
from .models import Transaction, Merchant
from .vitals import VitalService

from flask import request


class WatchController(ApiView):
    methods = ['GET']

    def run(self, deps):
        s = VitalService()
        for tx in deps.get('db_session').query(Transaction).all():
            s.append_transaction(tx)

        return s.calculate()


class TransactionController(ApiView):
    methods = ['POST']

    def run(self, deps):
        tx_id = request.json['data']['id']
        merchant_id = request.json['data']['merchant']['id']

        db_session = deps.get('db_session')
        if db_session.query(Transaction).filter_by(tx_id=tx_id).count() > 0:
            # Skip processing transactions we already have
            return {}

        merchant = db_session.query(Merchant).filter_by(merchant_id=merchant_id).first()
        if merchant is None:
            # Skip processing - we don't know what to do with this
            return {}

        tx = Transaction(tx_id=tx_id, merchant=merchant, amount=round(abs(request.json['data']['amount'])/100, 2))
        db_session.add(tx)
        db_session.commit()

        return {
            'success': True
        }