from flask import _app_ctx_stack as stack
from flask import jsonify
from flask.views import View

from .database import create_session


class SQLAlchemy:
    def __init__(self, app=None, engine=None):
        self.app = app
        self.injected = None
        self.engine = engine
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.teardown_appcontext(self.teardown)

    def connect(self):
        return create_session(self.engine)

    def teardown(self, exception):
        ctx = stack.top
        if hasattr(ctx, 'sqlalchemy_session'):
            ctx.sqlalchemy_session.close()

    @property
    def session(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'sqlalchemy_session'):
                ctx.sqlalchemy_session = self.connect()
            return ctx.sqlalchemy_session


class DependencyInjectionContainer:
    def __init__(self):
        """
        Dependency injection container
        :return:
        """
        self._deps = {}

    def add(self, name, obj, fn=None):
        self._deps[name] = (obj, fn)

    def get(self, name):
        if self._deps[name][1] is None:
            # Simple dependency - return just the dependency
            return self._deps[name][0]
        else:
            # Complex dependency - call the lambda to create the dependency deferred
            if self._deps[name][1].__code__.co_argcount == 2:
                # Pass injection container so that this dependency can fetch it's own deps
                return self._deps[name][1](self._deps[name][0], self)
            else:
                return self._deps[name][1](self._deps[name][0])


class ApiView(View):
    decorators = []

    def __init__(self, deps):
        self.deps = deps

    def run(self, deps):
        return {}

    def dispatch_request(self):
        return jsonify(self.run(self.deps))