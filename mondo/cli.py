import click
from .extensions import DependencyInjectionContainer

from mondo import config as app_config
from .database import get_engine, create_session
from .flask import create_app
from .models import Base, Merchant, Transaction

click.disable_unicode_literals_warning = True


def make_dependencies(config):
    engine = get_engine(
        config.DATABASE_FILE,
    )

    deps = DependencyInjectionContainer()
    deps.add('config', config)
    deps.add('db_engine', engine)
    deps.add('db_session', create_session(engine))

    return deps

pass_dependencies = click.make_pass_decorator(DependencyInjectionContainer)


def fixtures():
    return (
        Merchant(name="McDonald's", merchant_id="merch_000094K1Qo2QZZfpp1g6wz",
                 fat=5, alcohol=0, caffeine=0, happy=4),
        Merchant(name="Pret", merchant_id="merch_00009438teihh17R68Cu6D",
                 fat=2, alcohol=0, caffeine=0, happy=2),
        Merchant(name="Starbucks", merchant_id="merch_00009438xwcB6PwBRjQTsP",
                 fat=0, alcohol=0, caffeine=2, happy=1),
        Merchant(name="Sainsbury", merchant_id="merch_0000943C1Ks7ffu1oPmxbF",
                 fat=0, alcohol=3, caffeine=0, happy=2),
    )


@click.group()
@click.pass_context
def cli(ctx):
    ctx.obj = make_dependencies(app_config)


@click.command()
@pass_dependencies
def createdb(deps):
    """Create the database if non existing"""
    engine = deps.get('db_engine')
    Base.metadata.create_all(engine)

    session = create_session(engine)
    session.commit()


@click.command()
@pass_dependencies
def resetdb(deps):
    """Re-create the database from scratch"""
    engine = deps.get('db_engine')
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # Insert fixtures
    session = deps.get('db_session')
    for obj in fixtures():
        session.add(obj)

    session.commit()



@click.command()
@pass_dependencies
def run(deps):
    """Run the development server"""
    app, db = create_app('mondo', deps.get('db_engine'), app_config)
    app.run(debug=True, host='0.0.0.0')


@click.command()
@pass_dependencies
def test(deps):
    print(deps.get('db_session').query(Transaction).all())

cli.add_command(resetdb)
cli.add_command(createdb)
cli.add_command(run)
cli.add_command(test)