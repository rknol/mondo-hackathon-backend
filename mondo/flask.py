from flask import Flask, jsonify
from .extensions import SQLAlchemy, DependencyInjectionContainer

from .controllers import TransactionController, WatchController


def create_app(name, engine_factory, config):
    # Bootstrap the flask app
    app = Flask(name)
    app.debug = config.APP_DEBUG
    db = SQLAlchemy(app, engine_factory)

    deps = DependencyInjectionContainer()
    deps.add('config', config)
    deps.add('db_session', db, lambda db: db.session) # Thread-local member 'session' not available at this time

    app.add_url_rule('/watch', view_func=WatchController.as_view('watch', deps=deps))
    app.add_url_rule('/transaction', view_func=TransactionController.as_view('transaction', deps=deps))

    @app.after_request
    def after_request(response):
      response.headers.add('Access-Control-Allow-Origin', '*')
      response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
      response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
      return response

    return app, db
