from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def get_engine(db_file):
    return create_engine('sqlite://{}'.format(db_file))


def create_session(engine):
    Session = sessionmaker(bind=engine)
    return Session()