"""
Setuptools setup script
"""
from setuptools import setup, find_packages

requires = [
    'Flask==0.10.1',
    'Jinja2==2.8',
    'SQLAlchemy==1.0.11',
    'click==6.2',
    'requests==2.9.1'
]

setup(name='mondohack',
      version='0.1',
      description='Our project',
      author='Ruben Knol',
      author_email='c.minor6@gmail.com',
      license='Proprietary',
      packages=find_packages(),
      install_requires=requires,
      entry_points='''
        [console_scripts]
        cli.py=mondo.cli:cli
    ''',
      zip_safe=False)
