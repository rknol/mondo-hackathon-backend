# Mondo Hackathon

## Installation

### Requirements

* Python 3.*
* Python virtualenv

### Installation

You need to create a new virtualenv and activate it:

```
$ pyvenv ./venv
$ source ./venv/bin/activate
```

Create the database tables:

```
$ cli.py createdb
```

Run the development server:

```
$ cli.py run
```